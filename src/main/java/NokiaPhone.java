interface PhoneConnection {
    public void call();
    public void sendMessage();
}

class NokiaPhone implements PhoneConnection {
    public void call() {

        System.out.println("Phone can call");
    }
    public void sendMessage() {
        // The body of sleep() is provided here
        System.out.println("Phone can send message");
    }

    public static void main(String[] args) {
        NokiaPhone NokiaPhone = new NokiaPhone();
        NokiaPhone.call();
        NokiaPhone.sendMessage();
    }
}

