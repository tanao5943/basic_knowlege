public class Phone {
    public static int name;
    public int model;
    public String _maker;
    public double _price;
    public String _year;
    public String _color;

    public Phone() {
        name++;
        model = name;

    }
    @Override
    public String toString() {
        return "Phone" + model + " " + _maker + " " + _color + " " + _price + " " + _year + " ";

    }
    public static void main(String[] args) {

        Phone SamsungPhone = new Phone();
        SamsungPhone._maker = "Samsung";
        SamsungPhone._price = 10000;
        SamsungPhone._year = "2000";
        SamsungPhone._color = "black";

        Phone NokiaPhone = new Phone();
        NokiaPhone._maker = "Nokia";
        NokiaPhone._price = 1000;
        NokiaPhone._year = "2010";
        NokiaPhone._color = "black";

        System.out.println(SamsungPhone);
        System.out.println(NokiaPhone);

    }
}