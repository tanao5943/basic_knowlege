        interface PhoneMedia {
        public void makePhoto();// interface method
        public void makeVideo();
    }

    class SamsungPhone implements PhoneMedia {
        public void makePhoto() {

            System.out.println("Phone can make photo");
        }
        public void makeVideo() {
            // The body of sleep() is provided here
            System.out.println("Phone can make video");
        }
    }

    class Main {
        public static void main(String[] args) {
            SamsungPhone SamsungPhone = new SamsungPhone();
            SamsungPhone.makePhoto();
            SamsungPhone.makeVideo();
        }
    }
